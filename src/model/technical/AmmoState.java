package model.technical;

public enum AmmoState {
    
    AVAILABLE,
    CHOSEN,
    AIMING,
    FLYING,
    TARGET_JUST_HIT,
    TARGET_JUST_MISSED,
    TARGET_HAS_BEEN_HIT,
    TARGET_HAS_BEEN_MISSED,
    ALREADY_USED;

}
