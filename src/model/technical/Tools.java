package model.technical;

import java.awt.Image;
import java.awt.geom.AffineTransform;
import java.util.concurrent.ThreadLocalRandom;

import model.business.Movable;

public class Tools {
    
    public static boolean hasHit(Movable movable1, Movable movable2) {
        final int divisor = 3;
        double ammoX1 = movable1.getX()-movable1.getWidth()/divisor;
        double ammoY1 = movable1.getY()+movable1.getHeigth()/divisor;
        double ammoX2 = movable1.getX()+movable1.getWidth()/divisor;
        double ammoY2 = movable1.getY()-movable1.getHeigth()/divisor;
        
        double targetX1 = movable2.getX()-movable2.getWidth()/divisor;
        double targetY1 = movable2.getY()+movable2.getHeigth()/divisor;
        double targetX2 = movable2.getX()+movable2.getWidth()/divisor;
        double targetY2 = movable2.getY()-movable2.getHeigth()/divisor;
        
        //Check using diagonals
        return !(targetX1 > ammoX2 ||
                 targetY1 < ammoY2 ||
                 ammoX1 > targetX2 ||
                 ammoY1 < targetY2);
    }
    
    public static AffineTransform createAffineTransform(Movable movable, Image img, boolean reverted) {
        AffineTransform tx = new AffineTransform();
        
        double angleOffset = reverted ? - Math.PI/2 : Math.PI/2;
        
        //place the image on bird location
        tx.translate((int)movable.getX(), (int)movable.getY());
        //calculate the angle rotation, according to the bird's direciton
        double rotationAngle = - (Math.atan2(movable.getVelocity().getX(),movable.getVelocity().getY()) - angleOffset);
        //to the rotation
        tx.rotate(rotationAngle);
        //translate to the center of the image
        tx.translate(-img.getWidth(null)/2, -img.getHeight(null)/2);
        return tx;
    }
    
    public static int random(int min, int max) {
        return ThreadLocalRandom.current().nextInt(min, max + 1);
    }

}
