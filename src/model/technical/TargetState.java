package model.technical;

public enum TargetState {

    ALIVE,
    HIT,
    DEAD;
    
}
