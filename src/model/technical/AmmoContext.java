package model.technical;

import java.util.List;

import model.business.Constraint;
import model.business.decor.Decor;

public class AmmoContext implements Context {

    private AmmoState ammoState;
    private List<Constraint> constraints;
    private List<Decor> decors;
    
    
    public AmmoContext(List<Constraint> constraints, List<Decor> decors) {
        this.ammoState = AmmoState.AVAILABLE;
        this.constraints = constraints;
        this.decors = decors;
    }

    public AmmoState getAmmoState() {
        return ammoState;
    }
    public void setAmmoState(AmmoState ammoState) {
        this.ammoState = ammoState;
    }
    public List<Constraint> getConstraints() {
        return constraints;
    }
    public void setConstraints(List<Constraint> constraints) {
        this.constraints = constraints;
    }
    public List<Decor> getDecors() {
        return decors;
    }
    public void setDecors(List<Decor> decors) {
        this.decors = decors;
    }


}
