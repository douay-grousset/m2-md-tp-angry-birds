package model.technical;

public enum GameState {

    NOT_EXISTING,
    IN_PROGRESS,
    LOST,
    WON
    
}
