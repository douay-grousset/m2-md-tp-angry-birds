package model.technical;

import model.business.Level;

public class TargetContext implements Context {

    private TargetState targetState;
    private Level level;
    
    public TargetContext(Level level) {
        this.targetState = TargetState.ALIVE;
        this.level = level;
    }
    
    public TargetState getTargetState() {
        return targetState;
    }
    public void setTargetState(TargetState targetState) {
        this.targetState = targetState;
    }
    public Level getLevel() {
        return level;
    }
    public void setLevel(Level level) {
        this.level = level;
    }

}
