package model.technical;

import java.awt.Graphics2D;
import java.util.List;
import java.util.Observable;
import java.util.concurrent.locks.ReentrantLock;

import model.business.Constraint;
import model.business.Level;
import model.business.ammo.Ammo;
import model.business.decor.Decor;
import model.business.target.Target;

public class Game extends Observable implements Runnable {

    private String message;                             // message à afficher en haut de l'écran
    private int score;                                  // nombre de fois où le joueur a gagné
    private int nbRounds;
    private Level level;
    private List<Ammo> ammos;
    private List<Target> targets;
    private List<Constraint> constraint;
    private List<Decor> decors;
    private GameState state;
    private ReentrantLock lock;
    private boolean loop;
    
    private int currentAmmoIndex;
    
    public Game(Level level) {
        this.score = 0;
        this.level = level;
        this.nbRounds = 0;
        this.lock = new ReentrantLock();
        this.loop = true;
        ammos = level.getAmmo();
        targets = level.getTargets();
        decors = level.getDecors();
        level.getSound().play();
        init();
    }
    
    private void init() {
        lock.lock();
        message = "Choisissez l'angle et la vitesse.";
        currentAmmoIndex = 0;
        
        ammos = level.getAmmo();
        targets = level.getTargets();
        constraint = level.getConstraints();
        decors = level.getDecors();
        
        ammos.forEach(a -> a.setContext(new AmmoContext(constraint,decors)));
        targets.forEach(t -> t.setContext(new TargetContext(level)));
        
        state = GameState.IN_PROGRESS;
        ammos.get(currentAmmoIndex).setState(AmmoState.AIMING);
        lock.unlock();
    }

    public String getMessage() {
        synchronized (lock) {
            return message;
        }
    }
    
    public Level getLevel() {
        return level;
    }

    public int getScore() {
        synchronized (lock) {
            return score;
        }
    }
    
    public GameState getGameState() {
        synchronized (lock) {
            return state;
        }
    }
    
    public int getNbRounds() {
        synchronized (lock) {
            return nbRounds;
        }
    }

    public void loadNextAmmo() {
        synchronized (lock) {
            if(currentAmmoIndex+1 >= ammos.size()) {
                return;
            }
            
            ammos.get(currentAmmoIndex).setState(AmmoState.ALREADY_USED);
            
            currentAmmoIndex++;
            
            for(Target target : targets) {
                if(target.getState() == TargetState.HIT) {
                    target.setState(TargetState.DEAD);
                }
            }
              //TODO add this check in controller
    //        if(shot.getState().isArrived() == false) {
    //            throw new RuntimeException("Preparing a new shot is not allowed if the previous one hasn't arrived.");
    //        }
            
            ammos.get(currentAmmoIndex).setState(AmmoState.AIMING);
        }
    }

    public void shoot(double velocityX, double velocityY) {
        lock.lock();
        if(currentAmmoIndex >= ammos.size()) {
            throw new RuntimeException("Can't shoot when no ammo is remaining");
        }
        ammos.get(currentAmmoIndex).setState(AmmoState.FLYING);
        ammos.get(currentAmmoIndex).getVelocity().setX(velocityX);
        ammos.get(currentAmmoIndex).getVelocity().setY(velocityY);
        lock.unlock();
    }
    
    public void playAgain() {
        lock.lock();
        init();
        lock.unlock();
    }

     public List<Ammo> getAmmos() {
        synchronized (lock) {
            return ammos;
        }
    }

    public List<Target> getTargets() {
        synchronized (lock) {
            return targets;
        }
    }

    public Ammo getCurrentAmmo() {
        synchronized (lock) {
            return ammos.get(currentAmmoIndex);
        }
    }
    
    public void updateCurrentAmmoVelocity(double newVelocityX, double newVelocityY) {
        lock.lock();
        getCurrentAmmo().getVelocity().setX(newVelocityX);
        getCurrentAmmo().getVelocity().setY(newVelocityY);
        lock.unlock();
    }

    public void repaint(Graphics2D g) {
        lock.lock();
        level.getBackground().draw(g);
        decors.forEach(d -> d.draw(g));
        ammos.forEach(a -> a.draw(g));
        targets.forEach(t -> t.draw(g));
        lock.unlock();
    }

    @Override
    public void run() {
        while(loop == true){
            lock.lock();
            ammos.forEach(ammmo -> ammmo.doAction() );
            targets.forEach(target -> target.doAction() );
            updateStatesAccordingToCollisions();
            doActionsAccordingToStates();
            lock.unlock();
            
            setChanged();
            notifyObservers();
            try {
                Thread.sleep(10);
            } catch (InterruptedException e) {
                loop = false;
                lock.unlock();
            }
        }
    }
    
    public void stopGame() {
        lock.lock();
        loop = false;
        level.getSound().stop();
        lock.unlock();
    }

    private void updateStatesAccordingToCollisions() {
        for(Ammo ammo : ammos) {
            double ammoX = ammo.getX();
            double ammoY = ammo.getY();
            
            if(ammo.getState() == AmmoState.FLYING) {
                if(ammoX < 20 || ammoX > 780 || ammoY < 0 || ammoY > 510) {
                        ammo.setState(AmmoState.TARGET_JUST_MISSED);
                }
                for(Target target : targets) {
                    if(target.getState() == TargetState.ALIVE) {
                        // conditions de victoire
                        if(Tools.hasHit(ammo, target)) {
                            target.setState(TargetState.HIT);
                            ammo.setState(AmmoState.TARGET_JUST_HIT);
                            break;
                        }
                    }
                }
                for(Decor decor : decors) {
                    if(Tools.hasHit(ammo, decor)) {
                        ammo.setState(AmmoState.TARGET_JUST_MISSED);
                        break;
                    }
                }
            }
        }
    }
    
    private void doActionsAccordingToStates() {

        Ammo currentAmmo = getCurrentAmmo();
        AmmoState currentAmmoState = currentAmmo.getState();
        switch(currentAmmoState) {
            case AVAILABLE:
            case CHOSEN:
            case AIMING:
                message = "Choisissez l'angle et la vitesse.";
                break;
            case FLYING:
                message = "L'oiseau prend sont envol";
                break;
            case TARGET_JUST_MISSED:
                if(remainingAvailableAmmo() > 0) {
                    message = "Loupé !";
                }
                else {
                    message = "Perdu : vous n'avez plus d'oiseaux";
                    nbRounds++;
                    state = GameState.LOST;
                }
                currentAmmo.setState(AmmoState.TARGET_HAS_BEEN_MISSED);
                break;
            case TARGET_JUST_HIT:
                if(remainingPigs() != 0) {
                    if(remainingAvailableAmmo() > 0) {
                        message = "Bien joué ! Un cochon de moins";
                    }
                    else {
                        message = "Perdu : vous n'avez plus d'oiseaux";
                        nbRounds++;
                        state = GameState.LOST;
                    }
                }
                else {
                    score ++;
                    message = "Bien joué ! Vous avez eu tous les cochons";
                    nbRounds++;
                    state = GameState.WON;
                }
                currentAmmo.setState(AmmoState.TARGET_HAS_BEEN_HIT);
                break;
            case TARGET_HAS_BEEN_HIT:
            case TARGET_HAS_BEEN_MISSED:
            case ALREADY_USED:
                break;
            default:
                throw new RuntimeException("Unknown enum value.");
        }
    }
    
    public int getRemainingAmmo() {
        synchronized (lock) {
            int cpt = 0;
            for(Ammo itAmmo : ammos) {
                if(itAmmo.getState() == AmmoState.AVAILABLE || itAmmo.getState() == AmmoState.CHOSEN || itAmmo.getState() == AmmoState.AIMING) {
                    cpt++;
                }
            }
            return cpt;
        }
    }
    
    private int remainingPigs() {
        int cpt = 0;
        for(Target target : targets) {
            if(target.getState() == TargetState.ALIVE) {
                cpt++;
            }
        }
        return cpt;
    }
    
    private int remainingAvailableAmmo() {
        int cpt = 0;
        for(Ammo itAmmo : ammos) {
            if(itAmmo.getState() == AmmoState.AVAILABLE) {
                cpt++;
            }
        }
        return cpt;
    }

}
