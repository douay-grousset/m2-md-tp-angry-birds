package model.business.target.drawer;

import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.Toolkit;

import model.business.target.Target;
import model.technical.TargetContext;

public class SkullTargetDrawer implements TargetDrawer {

    private Image imgIdle;
    private Image imgHit;
    private Image imgIdleRev;
    private Image imgHitRev;
    
    @Override
    public void defineTarget(Target target) {
        Toolkit toolkit = Toolkit.getDefaultToolkit();
        this.imgIdle = toolkit.createImage("ressources/targets/skull-idle.png").getScaledInstance(target.getWidth(), target.getHeigth(), Image.SCALE_SMOOTH);
        this.imgHit = toolkit.createImage("ressources/targets/skull-hit.png").getScaledInstance(target.getWidth(), target.getHeigth(), Image.SCALE_SMOOTH);
        this.imgIdleRev = toolkit.createImage("ressources/targets/skull-idle-rev.png").getScaledInstance(target.getWidth(), target.getHeigth(), Image.SCALE_SMOOTH);
        this.imgHitRev = toolkit.createImage("ressources/targets/skull-hit-rev.png").getScaledInstance(target.getWidth(), target.getHeigth(), Image.SCALE_SMOOTH);
    }
    
    @Override
    public void draw(Graphics2D g, TargetContext context, Target target) {
        switch(context.getTargetState()) {
            case ALIVE:
                if(target.getVelocity().getX() < 0) {
                    g.drawImage(imgIdle, (int)target.getX(), (int)target.getY(), null);
                }
                else {
                    g.drawImage(imgIdleRev, (int)target.getX(), (int)target.getY(), null);
                }
                break;
            case HIT:
                if(target.getVelocity().getX() < 0) {
                    g.drawImage(imgHit, (int)target.getX(), (int)target.getY(), null);
                }
                else {
                    g.drawImage(imgHitRev, (int)target.getX(), (int)target.getY(), null);
                }
                break;
            case DEAD:
                break;
            default:
                break;      
        }
    }

    @Override
    public TargetDrawer clone() {
        return new SkullTargetDrawer();
    }
    
}
