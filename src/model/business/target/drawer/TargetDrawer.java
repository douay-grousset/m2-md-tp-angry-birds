package model.business.target.drawer;

import java.awt.Graphics2D;

import model.business.target.Target;
import model.technical.TargetContext;

public interface TargetDrawer extends Cloneable {

    public void defineTarget(Target target);
    
    public void draw(Graphics2D g, TargetContext context, Target target);

    public TargetDrawer clone();
    
}
