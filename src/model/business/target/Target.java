package model.business.target;

import java.awt.Graphics2D;

import model.business.Movable;
import model.business.Velocity;
import model.business.target.behavior.TargetBehavior;
import model.business.target.drawer.TargetDrawer;
import model.technical.TargetContext;
import model.technical.TargetState;

public class Target implements Movable, Cloneable {

    private double x;
    private double y;
    private int width;
    private int heigth;
    private Velocity velocity;
    private TargetContext context;
    private TargetBehavior behavior;
    private TargetDrawer drawer;

    public Target(double x, double y, int width, int heigth, TargetBehavior behavior, TargetDrawer drawer) {
        this.x = x;
        this.y = y;
        this.width = width;
        this.heigth = heigth;
        this.behavior = behavior;
        this.drawer = drawer;
        
        behavior.defineTarget(this);
        drawer.defineTarget(this);
    }
    
    public TargetContext getContext() {
        return context;
    }

    public void setContext(TargetContext context) {
        this.context = context;
    }

    @Override
    public double getX() {
        return x;
    }

    @Override
    public void setX(double x) {
        this.x = x;
    }

    @Override
    public double getY() {
        return y;
    }

    @Override
    public void setY(double y) {
        this.y = y;
    }
    
    @Override
    public int getWidth() {
        return width;
    }

    @Override
    public int getHeigth() {
        return heigth;
    }
    
    @Override
    public Velocity getVelocity() {
        return velocity;
    }

    @Override
    public void setVelocity(Velocity velocity) {
        this.velocity = velocity;
    }

    public TargetState getState() {
        return context.getTargetState();
    }

    public void setState(TargetState state) {
        context.setTargetState(state);
    }
   
    public void draw(Graphics2D g) {
        drawer.draw(g, context, this);
    }

    public void doAction() {
        behavior.doAction(context, this);
    }

    @Override
    public Target clone() {
        return new Target(x, y, width, heigth, behavior.clone(), drawer.clone());
    }
    
}
