package model.business.target.behavior;

import model.business.Velocity;
import model.business.target.Target;
import model.technical.TargetContext;

public class StaticTargetBehavior implements TargetBehavior {

    @Override
    public void defineTarget(Target target) {
        target.setVelocity(new Velocity(-1, 0));
    }
    
    @Override
    public void doAction(TargetContext context, Target target) {
        switch(context.getTargetState()) {
            case ALIVE:
                break;
            case HIT:
                break;
            case DEAD:
                break;
            default:
                break;
        }
    }
    
    @Override
    public StaticTargetBehavior clone() {
        return new StaticTargetBehavior();
    }
    
}
