package model.business.target.behavior;

import model.business.Velocity;
import model.business.target.Target;
import model.technical.TargetContext;

public class VerticalTargetBehavior implements TargetBehavior {

    private final int nbItBeforeReset = 300;
    private int itAction = 1;
    
    @Override
    public void defineTarget(Target target) {
        target.setVelocity(new Velocity(-1, 1));
    }
    
    @Override
    public void doAction(TargetContext context, Target target) {
        switch(context.getTargetState()) {
            case ALIVE:
                if(itAction == nbItBeforeReset) itAction = 1;
                
                double targetY = target.getY();
                Velocity velocity = target.getVelocity();

                double newTargetY = targetY + velocity.getY();
                target.setY(newTargetY);
                
                if(itAction++ == nbItBeforeReset/2) {
                    target.getVelocity().setY(- target.getVelocity().getY());
                }
                break;
            case HIT:
                break;
            case DEAD:
                break;
            default:
                break;
        }
    }

    @Override
    public VerticalTargetBehavior clone() {
        return new VerticalTargetBehavior();
    }

}
