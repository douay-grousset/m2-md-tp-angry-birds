package model.business.target.behavior;

import model.business.target.Target;
import model.technical.TargetContext;

public interface TargetBehavior extends Cloneable {

    public void defineTarget(Target target);
    
    public void doAction(TargetContext context, Target target);

    public TargetBehavior clone();
    
}
