package model.business.target.behavior;

import model.business.Velocity;
import model.business.target.Target;
import model.technical.TargetContext;

public class HorizontalTargetBehavior implements TargetBehavior {

    private final int nbItBeforeReset = 200;
    private int itAction = 1;
    
    @Override
    public void defineTarget(Target target) {
        target.setVelocity(new Velocity(1, 0));
    }
    
    @Override
    public void doAction(TargetContext context, Target target) {
        switch(context.getTargetState()) {
            case ALIVE:
                if(itAction == nbItBeforeReset) itAction = 1;
                
                double targetX = target.getX();
                double targetY = target.getY();
                Velocity velocity = target.getVelocity();

                double newTargetX = targetX + velocity.getX();
                double newTargetY = targetY + velocity.getY();
                target.setX(newTargetX);
                target.setY(newTargetY);
                
                if(itAction++ == nbItBeforeReset/2) {
                    target.getVelocity().setX(- target.getVelocity().getX());
                }
                break;
            case HIT:
                break;
            case DEAD:
                break;
            default:
                break;
        }
    }
    
    @Override
    public HorizontalTargetBehavior clone() {
        return new HorizontalTargetBehavior();
    }

}
