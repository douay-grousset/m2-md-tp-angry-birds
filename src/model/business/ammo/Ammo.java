package model.business.ammo;

import java.awt.Graphics2D;

import model.business.Movable;
import model.business.Velocity;
import model.business.ammo.behavior.AmmoBehavior;
import model.business.ammo.drawer.AmmoDrawer;
import model.technical.AmmoContext;
import model.technical.AmmoState;

public class Ammo implements Movable, Cloneable {

    private double x;
    private double y;
    private int width;
    private int heigth;
    private Velocity velocity;
    private AmmoContext context;
    private AmmoBehavior behavior;
    private AmmoDrawer drawer;

    public Ammo(double x, double y, int width, int heigth, AmmoBehavior behavior, AmmoDrawer drawer) {
        this.x = x;
        this.y = y;
        this.width = width;
        this.heigth = heigth;
        this.behavior = behavior;
        this.drawer = drawer;
        
        behavior.defineAmmo(this);
        drawer.defineAmmo(this);
    }

    public AmmoContext getContext() {
        return context;
    }

    public void setContext(AmmoContext context) {
        this.context = context;
    }

    @Override
    public double getX() {
        return x;
    }

    @Override
    public void setX(double x) {
        this.x = x;
    }

    @Override
    public double getY() {
        return y;
    }

    @Override
    public void setY(double y) {
        this.y = y;
    }

    @Override
    public int getWidth() {
        return width;
    }

    @Override
    public int getHeigth() {
        return heigth;
    }

    @Override
    public Velocity getVelocity() {
        return velocity;
    }

    @Override
    public void setVelocity(Velocity velocity) {
        this.velocity = velocity;
    }

    public AmmoState getState() {
        return context.getAmmoState();
    }

    public void setState(AmmoState state) {
        this.context.setAmmoState(state);
    }

    public void draw(Graphics2D g) {
        drawer.draw(g, context, this);
    }

    public void doAction() {
        behavior.doAction(context, this);
    }
    
    @Override
    public Ammo clone() {
        return new Ammo(x, y, width, heigth, behavior.clone(), drawer.clone());
    }
}
