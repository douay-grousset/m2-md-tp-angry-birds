package model.business.ammo.drawer;

import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.Toolkit;

import model.business.ammo.Ammo;
import model.technical.AmmoContext;
import model.technical.Tools;

public class BirdAmmoDrawer implements AmmoDrawer {

    private Image imgIdle;
    private Image imgIdleRev;
    private Image imgFlying;
    private Image imgFlyingRev;
    private Image imgHit;
    private Image imgHitRev;
    
    @Override
    public void defineAmmo(Ammo ammo) {
        Toolkit toolkit = Toolkit.getDefaultToolkit();
        this.imgIdle = toolkit.createImage("ressources/ammo/bird-idle.png").getScaledInstance(ammo.getWidth(), ammo.getHeigth(), Image.SCALE_SMOOTH);
        this.imgIdleRev = toolkit.createImage("ressources/ammo/bird-idle-rev.png").getScaledInstance(ammo.getWidth(), ammo.getHeigth(), Image.SCALE_SMOOTH);
        
        this.imgFlying = toolkit.createImage("ressources/ammo/bird-flying.png").getScaledInstance(ammo.getWidth(), ammo.getHeigth(), Image.SCALE_SMOOTH);
        this.imgFlyingRev = toolkit.createImage("ressources/ammo/bird-flying-rev.png").getScaledInstance(ammo.getWidth(), ammo.getHeigth(), Image.SCALE_SMOOTH);
        
        this.imgHit = toolkit.createImage("ressources/ammo/bird-hit.png").getScaledInstance(ammo.getWidth(), ammo.getHeigth(), Image.SCALE_SMOOTH);
        this.imgHitRev = toolkit.createImage("ressources/ammo/bird-hit-rev.png").getScaledInstance(ammo.getWidth(), ammo.getHeigth(), Image.SCALE_SMOOTH);
    }

    @Override
    public void draw(Graphics2D g, AmmoContext context, Ammo ammo) {
        switch(context.getAmmoState()) {
            case AVAILABLE:
                break;
            case AIMING:
                if(ammo.getVelocity().getX() >= 0) {
                    g.drawImage(imgIdle, Tools.createAffineTransform(ammo,imgIdle,false), null);
                }
                else {
                    g.drawImage(imgIdleRev, Tools.createAffineTransform(ammo,imgIdleRev,true), null);
                }
                break;
            case CHOSEN:
                if(ammo.getVelocity().getX() >= 0) {
                    g.drawImage(imgIdle, Tools.createAffineTransform(ammo,imgIdle,false), null);
                }
                else {
                    g.drawImage(imgIdleRev, Tools.createAffineTransform(ammo,imgIdleRev,true), null);
                }
                break;
            case FLYING:
                if(ammo.getVelocity().getX() >= 0) {
                    g.drawImage(imgFlying, Tools.createAffineTransform(ammo,imgFlying,false), null);
                }
                else {
                    g.drawImage(imgFlyingRev, Tools.createAffineTransform(ammo,imgFlyingRev,true), null);
                }
                break;
            case TARGET_HAS_BEEN_HIT:
                if(ammo.getVelocity().getX() >= 0) {
                    g.drawImage(imgFlying, Tools.createAffineTransform(ammo,imgFlying,false), null);
                }
                else {
                    g.drawImage(imgFlyingRev, Tools.createAffineTransform(ammo,imgFlyingRev,true), null);
                }
                break;
            case TARGET_HAS_BEEN_MISSED:
                if(ammo.getVelocity().getX() >= 0) {
                    g.drawImage(imgHit, Tools.createAffineTransform(ammo,imgHit,false), null);
                }
                else {
                    g.drawImage(imgHitRev, Tools.createAffineTransform(ammo,imgHitRev,true), null);
                }
                break;
            case TARGET_JUST_HIT:
                if(ammo.getVelocity().getX() >= 0) {
                    g.drawImage(imgFlying, Tools.createAffineTransform(ammo,imgFlying,false), null);
                }
                else {
                    g.drawImage(imgFlyingRev, Tools.createAffineTransform(ammo,imgFlyingRev,true), null);
                }
                break;
            case TARGET_JUST_MISSED:
                if(ammo.getVelocity().getX() >= 0) {
                    g.drawImage(imgHit, Tools.createAffineTransform(ammo,imgHit,false), null);
                }
                else {
                    g.drawImage(imgHitRev, Tools.createAffineTransform(ammo,imgHitRev,true), null);
                }
                break;
            case ALREADY_USED:
                break;
            default:
                break;
        }
    }
    
    @Override
    public AmmoDrawer clone() {
        return new BirdAmmoDrawer();
    }
    
}
