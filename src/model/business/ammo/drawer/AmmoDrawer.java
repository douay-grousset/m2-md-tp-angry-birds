package model.business.ammo.drawer;

import java.awt.Graphics2D;

import model.business.ammo.Ammo;
import model.technical.AmmoContext;

public interface AmmoDrawer extends Cloneable {

    public void defineAmmo(Ammo ammo);
    
    public void draw(Graphics2D g, AmmoContext context, Ammo ammo);

    public AmmoDrawer clone();
    
}
