package model.business.ammo.behavior;

import model.business.Constraint;
import model.business.Velocity;
import model.business.ammo.Ammo;
import model.technical.AmmoContext;

public class ClassicAmmoBehavior implements AmmoBehavior {

    @Override
    public void defineAmmo(Ammo ammo) {
        ammo.setVelocity(new Velocity(1, 0));
    }

    @Override
    public void doAction(AmmoContext context, Ammo ammo) {
        switch(context.getAmmoState()) {
            case AVAILABLE:
                break;
            case CHOSEN:
                break;
            case AIMING:
                break;
            case FLYING:
                double ammoX = ammo.getX();
                double ammoY = ammo.getY();
                Velocity velocity = ammo.getVelocity();

                double newAmmoX = ammoX + velocity.getX();
                double newAmmoY = ammoY + velocity.getY();
                ammo.setX(newAmmoX);
                ammo.setY(newAmmoY);
                
                double newVelocityX = velocity.getX();
                double newVelocityY = velocity.getY();
                
                for(Constraint constraint : context.getConstraints()) {
                    newVelocityX += constraint.getVelocity().getX();
                    newVelocityY += constraint.getVelocity().getY();
                }
                
                ammo.getVelocity().setX(newVelocityX);
                ammo.getVelocity().setY(newVelocityY);
                break;
            case TARGET_HAS_BEEN_HIT:
                break;
            case TARGET_HAS_BEEN_MISSED:
                break;
            case TARGET_JUST_HIT:
                break;
            case TARGET_JUST_MISSED:
                break;
            case ALREADY_USED:
                break;
            default:
                break;
        }
    }

    @Override
    public ClassicAmmoBehavior clone() {
        return new ClassicAmmoBehavior();
    }

}
