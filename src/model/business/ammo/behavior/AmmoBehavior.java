package model.business.ammo.behavior;

import model.business.ammo.Ammo;
import model.technical.AmmoContext;

public interface AmmoBehavior extends Cloneable {

    public void defineAmmo(Ammo ammo);
    
    public void doAction(AmmoContext context, Ammo ammo);

    public AmmoBehavior clone();
    
}
