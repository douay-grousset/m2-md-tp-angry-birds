package model.business;

public class Constraint implements Cloneable  {

    private Velocity velocity;

    public Constraint(Velocity velocity) {
        super();
        this.velocity = velocity;
    }

    public Velocity getVelocity() {
        return velocity;
    }

    public void setVelocity(Velocity velocity) {
        this.velocity = velocity;
    }

    @Override
    protected Constraint clone() {
        return new Constraint(velocity.clone());
    }
    
}
