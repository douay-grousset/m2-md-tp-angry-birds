package model.business;

import java.io.File;
import java.io.IOException;

import javax.sound.sampled.AudioFormat;
import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.DataLine;
import javax.sound.sampled.LineUnavailableException;
import javax.sound.sampled.SourceDataLine;
import javax.sound.sampled.UnsupportedAudioFileException;

public class Sound {

    File file;
    private Thread thread = null;
    boolean loop;
    
    public Sound(String fileName) {
        this.file = new File(fileName);
    }
    
    public synchronized void play() {
        
        this.loop = true;
        
        thread = new Thread(new Runnable() {
            @Override
            public void run() {
                
                AudioInputStream in = null;
                AudioFormat baseFormat = null;
                AudioFormat decodedFormat = null;
                AudioInputStream stream = null;
                
                try {
                    in = AudioSystem.getAudioInputStream(file);
                    baseFormat = in.getFormat();
                    decodedFormat = new AudioFormat(AudioFormat.Encoding.PCM_SIGNED,
                                                    baseFormat.getSampleRate(),
                                                    16,
                                                    baseFormat.getChannels(),
                                                    baseFormat.getChannels() * 2,
                                                    baseFormat.getSampleRate(),
                                                    false);
                    stream =  AudioSystem.getAudioInputStream(decodedFormat, in);
                    
                    while(loop) {
                        rawplay(decodedFormat, stream);
                    }
                }
                catch (UnsupportedAudioFileException | IOException | LineUnavailableException e) {
                    System.err.println("Error on reading soundtrack");
                }
                finally {
                    try {
                        if(stream != null) {
                            stream.close();
                        }
                        if(in != null) {
                            in.close();
                        }
                    }
                    catch (IOException e) {
                        System.err.println("Error on closing soundtrack");
                    }
                }
            }
        });
        thread.start();
    }
    
    public synchronized void stop() {
        try {
            loop = false;
            thread.join();
        } catch (InterruptedException e) {
            System.err.println("Unexpected interrupt exception in soundtrack thread");
        }
    }
    
    void rawplay(AudioFormat targetFormat, AudioInputStream din) throws LineUnavailableException, IOException
    {
        SourceDataLine line = null;
        try {
            byte[] data = new byte[4096];
            line = getLine(targetFormat);
            if (line != null)
            {
              // Start
              line.start();
              int nBytesRead = 0;
              while (nBytesRead != -1 && loop)
              {
                  nBytesRead = din.read(data, 0, data.length);
                  if (nBytesRead != -1) line.write(data, 0, nBytesRead);
              }
            }
        }
        finally {
            // Stop
            if(line != null) {
                line.drain();
                line.stop();
                line.close();
            }
            try {
                din.close();
            } catch (IOException e) {
                System.err.println("Error in closing playback");
            }
        }
    }

    private static SourceDataLine getLine(AudioFormat audioFormat) throws LineUnavailableException
    {
      SourceDataLine res = null;
      DataLine.Info info = new DataLine.Info(SourceDataLine.class, audioFormat);
      res = (SourceDataLine) AudioSystem.getLine(info);
      res.open(audioFormat);
      return res;
    } 
    
}
