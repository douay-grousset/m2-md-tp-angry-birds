package model.business;

import java.util.ArrayList;
import java.util.List;

import model.business.ammo.Ammo;
import model.business.background.Background;
import model.business.decor.Decor;
import model.business.target.Target;

public class Level {

    private String name;
    private List<Ammo> ammo;
    private List<Target> targets;
    private List<Constraint> constraints;
    private List<Decor> decors;
    private Background background;
    private Sound sound;
    
    public Level(String name, List<Ammo> ammo, List<Target> targets, List<Constraint> constraints, List<Decor> decors, Background background, Sound sound) {
        if(ammo.size() < 1 || targets.size() < 1) {
            throw new RuntimeException("Invalid level parameters.");
        }
        this.name = name;
        this.ammo = ammo;
        this.targets = targets;
        this.constraints = constraints;
        this.decors = decors;
        this.background = background;
        this.sound = sound;
    }
    
    public String getName() {
        return name;
    }

    public List<Ammo> getAmmo() {
        List<Ammo> newList = new ArrayList<>();
        ammo.forEach(a -> newList.add(a.clone()));
        return newList;
        
    }

    public List<Target> getTargets() {
        List<Target> newList = new ArrayList<>();
        targets.forEach(t -> newList.add(t.clone()));
        return newList;
    }

    public List<Constraint> getConstraints() {
        List<Constraint> newList = new ArrayList<>();
        constraints.forEach(c -> newList.add(c.clone()));
        return newList;
    }

    public List<Decor> getDecors() {
        List<Decor> newList = new ArrayList<>();
        decors.forEach(d -> newList.add(d));
        return newList;
    }

    public Background getBackground() {
        return background;
    }

    public Sound getSound() {
        return sound;
    }
    
}
