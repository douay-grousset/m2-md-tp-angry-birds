package model.business.decor;

import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.Toolkit;

import model.business.Velocity;

public class MetalStickDecor implements Decor {

    private double x;
    private double y;
    private int width;
    private int heigth;
    private Velocity velocity;
    private Image img;

    public MetalStickDecor(double x, double y, int width, int heigth) {
        this.x = x;
        this.y = y;
        this.width = width;
        this.heigth = heigth;
        this.velocity = new Velocity(0, 0);
        this.img = Toolkit.getDefaultToolkit().createImage("ressources/decors/metal-stick.jpg").getScaledInstance(width, heigth, Image.SCALE_SMOOTH);
    }

    @Override
    public double getX() {
        return x;
    }

    @Override
    public void setX(double x) {
        this.x = x;
        
    }

    @Override
    public double getY() {
        return y;
    }

    @Override
    public void setY(double y) {
        this.y = y;
    }

    @Override
    public int getWidth() {
        return width;
    }

    @Override
    public int getHeigth() {
        return heigth;
    }

    @Override
    public Velocity getVelocity() {
        return velocity;
    }

    @Override
    public void setVelocity(Velocity velocity) {
        this.velocity = velocity;
    }
    
    @Override
    public void draw(Graphics2D g) {
        int topRightCornerX = (int)x-width/2;
        int topRightCornerY = (int)y-heigth/2;
        g.drawImage(img, topRightCornerX, topRightCornerY, null);
    }
    
}
