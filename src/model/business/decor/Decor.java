package model.business.decor;

import java.awt.Graphics2D;

import model.business.Movable;

public interface Decor extends Movable {

    public abstract void draw(Graphics2D g);
    
}
