package model.business;

import java.util.ArrayList;
import java.util.List;

import model.business.ammo.Ammo;
import model.business.ammo.behavior.ClassicAmmoBehavior;
import model.business.ammo.drawer.BirdAmmoDrawer;
import model.business.background.BeachBackground;
import model.business.background.CityBackground;
import model.business.background.FenceBackground;
import model.business.background.SpaceBackground;
import model.business.decor.WoodStickDecor;
import model.business.decor.AsteroidDecor;
import model.business.decor.Decor;
import model.business.decor.MetalStickDecor;
import model.business.target.Target;
import model.business.target.behavior.HorizontalTargetBehavior;
import model.business.target.behavior.StaticTargetBehavior;
import model.business.target.behavior.VerticalTargetBehavior;
import model.business.target.drawer.SkullTargetDrawer;
import model.technical.Tools;

public class LevelBuilder {
    
    public static ArrayList<Level> createLevels() {
        ArrayList<Level> levels = new ArrayList<>();
        
        levels.add(level1());
        levels.add(level2());
        levels.add(level3());
        levels.add(level4());
        levels.add(level5());
        levels.add(level6());
        levels.add(level7());
        
        return levels;
    }
    
    private static Level level1() {
        List<Ammo> ammo;
        List<Target> targets;
        List<Constraint> constraints;
        List<Decor> decors;
        
        ammo = new ArrayList<>();
        targets = new ArrayList<>();
        constraints = new ArrayList<>();
        decors = new ArrayList<>();
        ammo.add(new Ammo(100, 400, 50, 50, new ClassicAmmoBehavior(), new BirdAmmoDrawer()));
        ammo.add(new Ammo(100, 400, 50, 50, new ClassicAmmoBehavior(), new BirdAmmoDrawer()));
        ammo.add(new Ammo(100, 400, 50, 50, new ClassicAmmoBehavior(), new BirdAmmoDrawer()));
        targets.add(new Target(Tools.random(400, 750),480, 40, 40, new StaticTargetBehavior(), new SkullTargetDrawer()));
        constraints.add(new Constraint(new Velocity(0, 0.1)));
        decors.add(new WoodStickDecor(100,470,30,106));
        
        return new Level("Shoot it !", ammo, targets, constraints, decors, new BeachBackground(800, 600), new Sound("ressources/sounds/sound1.ogg"));
    }

    private static Level level2() {
        List<Ammo> ammo;
        List<Target> targets;
        List<Constraint> constraints;
        List<Decor> decors;
        
        ammo = new ArrayList<>();
        targets = new ArrayList<>();
        constraints = new ArrayList<>();
        decors = new ArrayList<>();
        ammo.add(new Ammo(100, 400, 50, 50, new ClassicAmmoBehavior(), new BirdAmmoDrawer()));
        ammo.add(new Ammo(100, 400, 50, 50, new ClassicAmmoBehavior(), new BirdAmmoDrawer()));
        targets.add(new Target(Tools.random(400, 750),480, 40, 40, new StaticTargetBehavior(), new SkullTargetDrawer()));
        targets.add(new Target(Tools.random(400, 750),480, 40, 40, new StaticTargetBehavior(), new SkullTargetDrawer()));
        constraints.add(new Constraint(new Velocity(0, 0.1)));
        decors.add(new WoodStickDecor(100,470,30,106));
        
        return new Level("Shoot them !", ammo, targets, constraints, decors, new BeachBackground(800, 600), new Sound("ressources/sounds/sound2.ogg"));
    }
    
    private static Level level3() {
        List<Ammo> ammo;
        List<Target> targets;
        List<Constraint> constraints;
        List<Decor> decors;
        
        ammo = new ArrayList<>();
        targets = new ArrayList<>();
        constraints = new ArrayList<>();
        decors = new ArrayList<>();
        ammo.add(new Ammo(100, 400, 50, 50, new ClassicAmmoBehavior(), new BirdAmmoDrawer()));
        ammo.add(new Ammo(100, 400, 50, 50, new ClassicAmmoBehavior(), new BirdAmmoDrawer()));
        targets.add(new Target(Tools.random(320, 670),480, 40, 40, new HorizontalTargetBehavior(), new SkullTargetDrawer()));
        constraints.add(new Constraint(new Velocity(0, 0.1)));
        decors.add(new WoodStickDecor(100,470,30,106));
        
        return new Level("It moves !", ammo, targets, constraints, decors, new BeachBackground(800, 600), new Sound("ressources/sounds/sound3.ogg"));
    }
    
    private static Level level4() {
        List<Ammo> ammo;
        List<Target> targets;
        List<Constraint> constraints;
        List<Decor> decors;
        
        ammo = new ArrayList<>();
        targets = new ArrayList<>();
        constraints = new ArrayList<>();
        decors = new ArrayList<>();
        ammo.add(new Ammo(100, 400, 50, 50, new ClassicAmmoBehavior(), new BirdAmmoDrawer()));
        ammo.add(new Ammo(100, 400, 50, 50, new ClassicAmmoBehavior(), new BirdAmmoDrawer()));
        targets.add(new Target(Tools.random(320, 670),480, 40, 40, new HorizontalTargetBehavior(), new SkullTargetDrawer()));
        targets.add(new Target(Tools.random(320, 670),480, 40, 40, new HorizontalTargetBehavior(), new SkullTargetDrawer()));
        constraints.add(new Constraint(new Velocity(0, 0.1)));
        decors.add(new WoodStickDecor(100,470,30,106));
        
        return new Level("They move !", ammo, targets, constraints, decors, new CityBackground(800, 600), new Sound("ressources/sounds/sound4.ogg"));
    }
    
    private static Level level5() {
        List<Ammo> ammo;
        List<Target> targets;
        List<Constraint> constraints;
        List<Decor> decors;
        
        ammo = new ArrayList<>();
        targets = new ArrayList<>();
        constraints = new ArrayList<>();
        decors = new ArrayList<>();
        ammo.add(new Ammo(100, 400, 50, 50, new ClassicAmmoBehavior(), new BirdAmmoDrawer()));
        ammo.add(new Ammo(100, 400, 50, 50, new ClassicAmmoBehavior(), new BirdAmmoDrawer()));
        ammo.add(new Ammo(100, 400, 50, 50, new ClassicAmmoBehavior(), new BirdAmmoDrawer()));
        targets.add(new Target(Tools.random(320, 670),480, 40, 40, new HorizontalTargetBehavior(), new SkullTargetDrawer()));
        targets.add(new Target(Tools.random(400, 750),300, 40, 40, new VerticalTargetBehavior(), new SkullTargetDrawer()));
        targets.add(new Target(Tools.random(400, 750),150, 40, 40, new VerticalTargetBehavior(), new SkullTargetDrawer()));
        constraints.add(new Constraint(new Velocity(0, 0.1)));
        constraints.add(new Constraint(new Velocity(-0.0275, 0)));
        decors.add(new WoodStickDecor(100,470,30,106));
        
        return new Level("With wind !", ammo, targets, constraints, decors, new FenceBackground(800, 600), new Sound("ressources/sounds/sound5.ogg"));
    }
    
    private static Level level6() {
        List<Ammo> ammo;
        List<Target> targets;
        List<Constraint> constraints;
        List<Decor> decors;
        
        ammo = new ArrayList<>();
        targets = new ArrayList<>();
        constraints = new ArrayList<>();
        decors = new ArrayList<>();
        ammo.add(new Ammo(100, 400, 50, 50, new ClassicAmmoBehavior(), new BirdAmmoDrawer()));
        ammo.add(new Ammo(100, 400, 50, 50, new ClassicAmmoBehavior(), new BirdAmmoDrawer()));
        ammo.add(new Ammo(100, 400, 50, 50, new ClassicAmmoBehavior(), new BirdAmmoDrawer()));
        targets.add(new Target(Tools.random(400, 750),300, 40, 40, new VerticalTargetBehavior(), new SkullTargetDrawer()));
        targets.add(new Target(Tools.random(400, 750),150, 40, 40, new VerticalTargetBehavior(), new SkullTargetDrawer()));
        decors.add(new MetalStickDecor(55,440,110,30));
        decors.add(new AsteroidDecor(400,550,800,100));
        
        return new Level("In space !", ammo, targets, constraints, decors, new SpaceBackground(800, 600), new Sound("ressources/sounds/sound6.ogg"));
    }
    
    private static Level level7() {
        List<Ammo> ammo;
        List<Target> targets;
        List<Constraint> constraints;
        List<Decor> decors;
        
        ammo = new ArrayList<>();
        targets = new ArrayList<>();
        constraints = new ArrayList<>();
        decors = new ArrayList<>();
        ammo.add(new Ammo(100, 400, 50, 50, new ClassicAmmoBehavior(), new BirdAmmoDrawer()));
        ammo.add(new Ammo(100, 400, 50, 50, new ClassicAmmoBehavior(), new BirdAmmoDrawer()));
        ammo.add(new Ammo(100, 400, 50, 50, new ClassicAmmoBehavior(), new BirdAmmoDrawer()));
        targets.add(new Target(Tools.random(320, 670), 100, 40, 40, new HorizontalTargetBehavior(), new SkullTargetDrawer()));
        targets.add(new Target(Tools.random(320, 670), 100, 40, 40, new HorizontalTargetBehavior(), new SkullTargetDrawer()));
        constraints.add(new Constraint(new Velocity(0, -0.1)));
        decors.add(new MetalStickDecor(55,440,110,30));
        decors.add(new AsteroidDecor(400,550,800,100));
        
        return new Level("Reverse gravity !", ammo, targets, constraints, decors, new SpaceBackground(800, 600), new Sound("ressources/sounds/sound6.ogg"));
    }
    
}
