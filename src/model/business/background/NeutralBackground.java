package model.business.background;

import java.awt.Color;
import java.awt.Graphics2D;

public class NeutralBackground extends Background {

    public NeutralBackground(int width, int heigth) {
        super(width, heigth);
    }

    @Override
    public void draw(Graphics2D g) {
        g.setColor(Color.WHITE);
        g.fillRect(0, 0, getWidth(), getHeight());
    }

}
