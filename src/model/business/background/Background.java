package model.business.background;

import java.awt.Graphics2D;

public abstract class Background {

    int width;
    int height;
    
    public Background(int width, int heigth) {
        super();
        this.width = width;
        this.height = heigth;
    }
    
    public int getWidth() {
        return width;
    }

    public void setWidth(int width) {
        this.width = width;
    }

    public int getHeight() {
        return height;
    }

    public void setHeight(int heigth) {
        this.height = heigth;
    }

    public abstract void draw(Graphics2D g);
    
}
