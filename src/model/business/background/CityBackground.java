package model.business.background;

import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.Toolkit;

public class CityBackground extends Background {

    private Image img = Toolkit.getDefaultToolkit().createImage("ressources/backgrounds/city.png");
    
    public CityBackground(int width, int heigth) {
        super(width, heigth);
    }

    @Override
    public void draw(Graphics2D g) {
        g.drawImage(img, 0, 0, 800,600, null);
    }
    
}
