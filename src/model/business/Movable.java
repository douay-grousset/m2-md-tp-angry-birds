package model.business;

public interface Movable {

    public double getX();
    
    public void setX(double x);

    public double getY();

    public void setY(double y);

    public int getWidth();

    public int getHeigth();

    public Velocity getVelocity();

    public void setVelocity(Velocity velocity);
    
}
