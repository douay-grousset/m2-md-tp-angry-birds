package view;

import java.awt.Frame;
import java.awt.event.ComponentEvent;
import java.awt.event.ComponentListener;
import java.util.concurrent.locks.ReentrantLock;

public class ResizeManager implements ComponentListener {

    private final double RATIO = 4/3.0;
    private Frame frame;
    private int previousWidth;
    private int previousHeight;
    private static ReentrantLock lock = new ReentrantLock();
    
    public ResizeManager(Frame frame) {
        this.frame = frame;
        previousWidth = (int)frame.getSize().getWidth();
        previousHeight = (int)frame.getSize().getHeight();
    }

    @Override
    public void componentHidden(ComponentEvent arg0) {}

    @Override
    public void componentMoved(ComponentEvent arg0) {}

    int tmpCounter = 0;
    boolean lastResizeTriggeredByUs = false;
    @Override
    public void componentResized(ComponentEvent event) {
        
//        if(lastResizeTriggeredByUs) {
//            System.out.println("################");
//            lastResizeTriggeredByUs = false;
//            return;
//        }
        
        if(lock.tryLock() == false) return;
        
        int newWidth = (int)frame.getSize().getWidth();
        int newHeight = (int)frame.getSize().getHeight();
        
        if(newWidth != previousWidth  && newHeight == previousHeight) {
            System.out.println("A");
            newHeight = (int)(newWidth/RATIO);
        }
        else if(newWidth == previousWidth && newHeight != previousHeight) {
            System.out.println("B");
            newWidth= (int)(newHeight*RATIO);
        }
        else if(newWidth != previousWidth && newHeight != previousHeight) {
            System.out.println("C");
            
            int diffWidth = Math.abs(newWidth-previousWidth);
            int diffHeight = Math.abs(newHeight-previousHeight);
            
            if(diffWidth > diffHeight*RATIO) {
                newHeight = (int)(newWidth/RATIO);
            }
            else {
                newWidth=(int)(newHeight*RATIO);
            }
        }
        else {
            System.out.println("D");
        }
        
        tmpCounter ++;
        System.out.println(previousWidth + "," + previousHeight + " => " + newWidth + "," + newHeight + " number of setSize calls: " + tmpCounter);
        
        lastResizeTriggeredByUs = true;
        frame.setSize(newWidth, newHeight);
        
        previousWidth = newWidth;
        previousHeight = newHeight;
        
        lock.unlock();
    }

    @Override
    public void componentShown(ComponentEvent arg0) {}
    
}
