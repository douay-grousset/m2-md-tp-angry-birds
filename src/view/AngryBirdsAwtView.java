package view;
import java.awt.*;
import java.awt.event.*;
import java.awt.geom.AffineTransform;
import java.util.Observable;
import java.util.Observer;

import controller.AngryBeardsController;
import model.business.Level;
import model.business.Movable;
import model.business.ammo.Ammo;
import model.technical.GameState;
import view.in_game_menu.InGameMenuPanel;
import view.level_menu.LevelMenuPopUpDialog;
import model.technical.AmmoState;

@SuppressWarnings("serial")
public class AngryBirdsAwtView extends Panel implements MouseListener, MouseMotionListener, Observer {
    
    private static final AngryBeardsController CONTROLLER = AngryBeardsController.getInstance();
    private int mouseX, mouseY;                         // position de la souris lors de la sélection
    private Image buffer;                               // image pour le rendu hors écran
    private Frame frame;
    private Image picImg = Toolkit.getDefaultToolkit().createImage("ressources/decors/pic.png").getScaledInstance(12, 24, Image.SCALE_SMOOTH);

    // constructeur
    AngryBirdsAwtView(Frame frame) {
        addMouseListener(this);
        addMouseMotionListener(this);
        CONTROLLER.subscribeToGame(this);
        this.frame = frame;
    }

    // gestion des événements souris
    @Override
    public void mouseClicked(MouseEvent e) { }
    @Override
    public void mouseEntered(MouseEvent e) { }
    @Override
    public void mouseExited(MouseEvent e) { }
    @Override
    public void mousePressed(MouseEvent e) { }
    @Override
    public void mouseReleased(MouseEvent e) {
        if(CONTROLLER.getGameState() == GameState.NOT_EXISTING)
            return;
        
        Ammo currentAmmo = CONTROLLER.getCurrentAmmo();
        if(currentAmmo.getState() == AmmoState.TARGET_JUST_HIT || currentAmmo.getState() == AmmoState.TARGET_JUST_MISSED) {
            return;
        }
        if(currentAmmo.getState() == AmmoState.TARGET_HAS_BEEN_HIT || currentAmmo.getState() == AmmoState.TARGET_HAS_BEEN_MISSED) {
            if(CONTROLLER.getGameState() == GameState.IN_PROGRESS) {
                CONTROLLER.loadNextAmmo();
            }
            else {
                CONTROLLER.playAgain();
            }
        } else if(currentAmmo.getState() == AmmoState.AIMING) {
            
            double ammoX = currentAmmo.getX();
            double ammoY = currentAmmo.getY();
            double newVelocityX = (ammoX - mouseX) / 20.0;
            double newVelocityY = (ammoY - mouseY) / 20.0;
            CONTROLLER.shoot(newVelocityX, newVelocityY);
            repaint();
        }
    }
    @Override
    public void mouseDragged(MouseEvent e) { mouseMoved(e); }
    @Override
    public void mouseMoved(MouseEvent e) { 
        mouseX = e.getX();
        mouseY = e.getY();
        repaint();
    }

    // évite les scintillements
    @Override
    public void update(Graphics g) {
        paint(g);
    }

    // dessine le contenu de l'écran dans un buffer puis copie le buffer à l'écran
    @Override
    public void paint(Graphics g2) {
        if(CONTROLLER.getGameState() == GameState.NOT_EXISTING)
            return;
        
        if(buffer == null) buffer = createImage(800, 600);
        Graphics2D g = (Graphics2D) buffer.getGraphics();

        CONTROLLER.repaintGame(g);

        Ammo currentAmmo = CONTROLLER.getCurrentAmmo();
        if(currentAmmo != null && currentAmmo.getState() == AmmoState.AIMING) {
            g.setColor(Color.BLACK);
            g.drawLine((int)currentAmmo.getX(), (int)currentAmmo.getY(), mouseX, mouseY);
            g.drawImage(picImg, createAffineTransform(currentAmmo, mouseX, mouseY, picImg), null);
            
            double newVelocityX = (currentAmmo.getX() - mouseX) / 20.0;
            double newVelocityY = (currentAmmo.getY() - mouseY) / 20.0;
            CONTROLLER.updateCurrentAmmoVelocity(newVelocityX,newVelocityY);
        }
        
        String frameName = "Angry birds";
        Level level = CONTROLLER.getCurrentLevel();
        if(level != null) {
            frameName += " : " + level.getName();
        }
        frame.setTitle(frameName);

        // affichage à l'écran sans scintillement
        g2.drawImage(buffer, 0, 0, null);
    }

    // taille de la fenêtre
    @Override
    public Dimension getPreferredSize() {
        return new Dimension(800, 600);
    }

    // met le jeu dans une fenêtre
    public static void main(String args[]) {
        Frame frame = new Frame("Angry birds");
        frame.setLayout(new BorderLayout());

        final AngryBirdsAwtView gameView = new AngryBirdsAwtView(frame);
        frame.add(gameView);

        final Panel gameMenu = new InGameMenuPanel(frame);
        frame.add(BorderLayout.NORTH, gameMenu);

        frame.pack();
        frame.setResizable(false);
        frame.setLocationRelativeTo(null);
        frame.setVisible(true);
        frame.addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosing(WindowEvent event) {
                System.exit(0);
            }
        });

        // pops the menu, needs to be done in the end when the size is set
        new LevelMenuPopUpDialog(frame);

    }

    @Override
    public void update(Observable arg0, Object arg1) {
        // redessine
        repaint();
    }
    
    private static AffineTransform createAffineTransform(Movable movable, int x, int y, Image img) {
        AffineTransform tx = new AffineTransform();
        
        //place the image on bird location
        tx.translate(x, y);
        //calculate the angle rotation, according to the bird's direciton
        double rotationAngle = - (Math.atan2(movable.getVelocity().getX(),movable.getVelocity().getY()));
        //to the rotation
        tx.rotate(rotationAngle);
        //translate to the center of the image
        tx.translate(-img.getWidth(null)/2, -img.getHeight(null)/2);
        return tx;
    }

}
