package view.level_menu;

import java.awt.BorderLayout;
import java.awt.Button;
import java.awt.Dialog;
import java.awt.Frame;
import java.awt.GridLayout;
import java.awt.Panel;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

import controller.AngryBeardsController;

@SuppressWarnings("serial")
public class LevelMenuPopUpDialog extends Dialog {

    public LevelMenuPopUpDialog(Frame parent) {
        super(parent, true); // modal: true

        this.addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosing(WindowEvent event) {
                System.exit(0);
            }
        });

        this.setLayout(new BorderLayout());

        Panel panel = createLevelList();
        this.add(panel);


        int widthPopup = 200;
        int heightPopup = 300;
        this.setSize(widthPopup, heightPopup);
        this.setLocation( // center
                // offset from parent position + center in parent
                parent.getLocation().x + (parent.getWidth() - widthPopup)/2,
                parent.getLocation().y + (parent.getHeight() - heightPopup)/2);
        this.setVisible(true);
    }

    private Panel createLevelList() {
        final AngryBeardsController CONTROLLER = AngryBeardsController.getInstance();
        final int NUMBER_OF_LEVELS = CONTROLLER.getLevels().size();

        Panel panel = new Panel();
        panel.setLayout(new GridLayout(NUMBER_OF_LEVELS, 1));

        for (int i = 0; i < NUMBER_OF_LEVELS; i++) {
            String levelName = CONTROLLER.getLevels().get(i).getName();
            Button level = new Button(levelName);
            level.addActionListener(new LevelMenuButtonActionListener(i, this));
            panel.add(level);
        }
        return panel;
    }

}
