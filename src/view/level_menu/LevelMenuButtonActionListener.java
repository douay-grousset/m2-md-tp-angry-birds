package view.level_menu;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import controller.AngryBeardsController;

public class LevelMenuButtonActionListener implements ActionListener {

    int levelIndex;
    private LevelMenuPopUpDialog menuDialog;

    private static final AngryBeardsController CONTROLLER = AngryBeardsController.getInstance();

    public LevelMenuButtonActionListener(int levelNumber, LevelMenuPopUpDialog menuDialog) {
        super();
        this.menuDialog = menuDialog;
        this.levelIndex = levelNumber;
    }

    @Override
    public void actionPerformed(ActionEvent arg0) {
        CONTROLLER.loadLevel(levelIndex);
        menuDialog.dispose();
    }

}
