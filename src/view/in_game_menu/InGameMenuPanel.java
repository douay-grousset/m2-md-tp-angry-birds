package view.in_game_menu;

import java.awt.Button;
import java.awt.Frame;
import java.awt.GridLayout;
import java.awt.Label;
import java.awt.Panel;
import java.util.Observable;
import java.util.Observer;

import controller.AngryBeardsController;
import view.level_menu.LevelMenuPopUpDialog;

@SuppressWarnings("serial")
public class InGameMenuPanel extends Panel implements Observer {
    
    private static final AngryBeardsController CONTROLLER = AngryBeardsController.getInstance();
    private Label labelRatioScoreRounds = new Label();
    private Label labelMessage = new Label();
    private Label labelRemainingAmmo = new Label();

    public InGameMenuPanel(Frame gameFrame) {
        this.setLayout(new GridLayout(1, 3));

        Button levelMenuButton = new Button("Level menu");
        levelMenuButton.addActionListener((ActionListener) -> {
                new LevelMenuPopUpDialog(gameFrame);
        });

        this.add(levelMenuButton);

        this.add(labelMessage);
        this.add(labelRatioScoreRounds);
        this.add(labelRemainingAmmo);

        CONTROLLER.subscribeToGame(this);
    }
    
    @Override
    public void update(Observable o, Object arg) {
        int score = CONTROLLER.getScore();
        int nbRounds = CONTROLLER.getNbRounds();
        String labelText = "win ratio: " + score + " / " + nbRounds;
        labelRatioScoreRounds.setText(labelText);
        labelMessage.setText(CONTROLLER.getMessage());
        labelRemainingAmmo.setText("remaining ammo: " + (CONTROLLER.getRemainingAmmo()));
        repaint();
    }

}
