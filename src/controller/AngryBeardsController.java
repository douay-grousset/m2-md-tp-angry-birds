package controller;

import java.awt.Graphics2D;
import java.util.HashSet;
import java.util.List;
import java.util.Observable;
import java.util.Observer;
import java.util.Set;

import model.business.Level;
import model.business.LevelBuilder;
import model.business.ammo.Ammo;
import model.business.target.Target;
import model.technical.Game;
import model.technical.GameState;

public class AngryBeardsController extends Observable {

    private static AngryBeardsController INSTANCE = new AngryBeardsController();

    public static AngryBeardsController getInstance() { return INSTANCE; }

    private Game game;
    private List<Level> levels;
    private Set<Observer> gameObservers;

    private AngryBeardsController()  {
        gameObservers = new HashSet<>();
        levels = LevelBuilder.createLevels();
    }
    
    public List<Level> getLevels() {
        return levels;
    }
    
    public Level getCurrentLevel() {
        if(game == null) {
            return null;
        }
        return game.getLevel();
    }
    
    public GameState getGameState() {
        if(game == null) {
            return GameState.NOT_EXISTING;
        }
        return game.getGameState();
        
    }
    
    public void loadLevel(int index) {
        if(index < 0 || index >= levels.size()) {
            throw new RuntimeException("Failed to load level. Invalid index");
        }
        startNewGame(index);
    }
    
    public void playAgain() {
        game.playAgain();
    }
    
    public int getNbRounds() {
        return game.getNbRounds();
    }

    public String getMessage() {
        return game.getMessage();
    }
    
    public List<Target> getTargets() {
        return game.getTargets();
    }

    public List<Ammo> getAmmos() {
        return game.getAmmos();
    }
    
    public int getScore() {
        return game.getScore();
    }
    
    public void subscribeToGame(Observer o) {
        gameObservers.add(o);
        if(game != null) {
            game.addObserver(o);
        }
    }
    
    public void loadNextAmmo() {
        if(game.getGameState() == GameState.IN_PROGRESS) {
            game.loadNextAmmo();
        }
    }
    
    public void shoot(double velocityX, double velocityY) {
        if(game.getGameState() == GameState.IN_PROGRESS) {
            final double capValue = 7;
            if(velocityX > capValue || velocityY > capValue) {
                double cappedVelocityX;
                double cappedVelocityY;
                if(velocityX > velocityY) {
                    cappedVelocityX = capValue;
                    cappedVelocityY = (velocityY/velocityX)*capValue;
                }
                else {
                    cappedVelocityX = (velocityX/velocityY)*capValue;
                    cappedVelocityY = capValue;
                }
                game.shoot(cappedVelocityX, cappedVelocityY);
            }
            else {
                game.shoot(velocityX, velocityY);
            }
        }
    }

    public Ammo getCurrentAmmo() {
        return game.getCurrentAmmo();
    }
    
    public int getRemainingAmmo() {
        return game.getRemainingAmmo();
    }
    
    public void updateCurrentAmmoVelocity(double newVelocityX, double newVelocityY) {
        if(game != null) {
            game.updateCurrentAmmoVelocity(newVelocityX,newVelocityY);
        }
    }
    
    private void startNewGame(int index) {
        if(game != null) {
            game.deleteObservers();
            //TODO check if this stopGame() method is enough to stop thread
            game.stopGame();
        }

        game = new Game(levels.get(index));
        
        gameObservers.forEach(o -> game.addObserver(o) );
        Thread t = new Thread(game);
        t.start();
    }

    public void repaintGame(Graphics2D g) {
        if(game != null) {
            game.repaint(g);
        }
    }

}
