# Work done compared to the initial version (non-exhaustive list)
## Switch MVC pattern
## One game has a score and various rounds
## Win ratio
Number of victories/number of rounds
## Multiple levels
## Window name changes with levels
## Different number of projectiles and targets
## Moving targets
Some horizontally, some vertically
## Win condition
To win, one must hit all the targets with all the given projectiles
## Every level is responsible to paint it's decorations and background
